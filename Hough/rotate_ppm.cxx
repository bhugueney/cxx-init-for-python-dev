#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <cmath>
#include <tuple>
#include <functional>
#include <limits>
#include <type_traits>
#include <chrono>
#include <ctime>

typedef std::vector<std::vector<std::array<unsigned char, 3>>> img_t;

std::istream& operator >>(std::istream& is, img_t& img)
{
  std::string header;
  is >> header;
  if (header.compare("P6") != 0){ throw("bad file format :"+ header);}
  // TODO should skip comments
  std::size_t w, h, max_col_val;
  is >> w >> h >> max_col_val; // we don't use max_col_Val, assuming 255
  is.get(); // skip the trailing white space
  //std::cerr<<"reading "<< header <<" w:"<<w<<" h:"<<h<<" max_col_val:"<<max_col_val<<std::endl;
  img.resize(h);
  unsigned char pix[3];
  for(std::size_t r=0; r != h; ++r){
    img[r].resize(w);
    for(std::size_t c=0; c != w; ++c){
      is.read(reinterpret_cast<char *>(pix), 3);
      img[r][c]= {pix[0], pix[1], pix[2]};
    }
  }
  return is;
}

std::ostream& operator <<(std::ostream& os, const img_t& img)
{
    os << "P6\n"
       << img[0].size() << " " << img.size() << "\n"
       << 255 << "\n";
    for(std::size_t r= 0; r != img.size(); ++r){
      for(std::size_t c= 0; c != img[r].size(); ++c){
	for(std::size_t channel=0; channel != img[r][c].size(); ++channel){
	  os << img[r][c][channel];
	}
      }
    }
    return os;
}

// using std::tuple instead of std::array<TC,2> to show the equivalent in python !
template<typename TA, typename TC>
std::tuple<TC, TC> rotate(TA angle, std::tuple<TC, TC> const& xy){
  TC x;
  TC y;
  std::tie(x,y)= xy;
  return std::tuple<TC, TC>(x*std::cos(angle) - y*std::sin(angle)
			    ,y*std::cos(angle) + x*std::sin(angle));
}

template<typename TC>
std::tuple<TC, TC> add(std::tuple<TC, TC> const& xy_1,std::tuple<TC, TC> const& xy_2){
  TC x_1;
  TC y_1;
  std::tie(x_1, y_1)= xy_1;
  TC x_2;
  TC y_2;
  std::tie(x_2, y_2)= xy_2;

  return std::make_tuple(x_1+x_2, y_1+y_2);
}

template<typename TS, typename TC>
std::tuple<TC, TC> scale(TS const k, std::tuple<TC, TC> const& xy){
  TC x;
  TC y;
  std::tie(x, y)= xy;
  return std::tuple<TC, TC>(k*x, k*y);
}

template<typename TA, typename TC>
std::tuple<TC, TC> rotate_around(TA angle, std::tuple<TC, TC> const& xy_c, std::tuple<TC, TC> const& xy){
  return add(xy_c, rotate(angle, add(scale(-1., xy_c), xy)));
}

template<typename TC>
std::tuple<TC, TC, TC, TC> update_min_max(std::tuple<TC, TC, TC, TC> xmin_xmax_ymin_y_max, std::tuple<TC, TC> xy){
  TC const x(std::get<0>(xy));
  TC const y(std::get<1>(xy));
  return std::make_tuple(std::min(std::get<0>(xmin_xmax_ymin_y_max), x)
			 ,std::max(std::get<1>(xmin_xmax_ymin_y_max), x)
			 ,std::min(std::get<2>(xmin_xmax_ymin_y_max), y)
			 ,std::max(std::get<3>(xmin_xmax_ymin_y_max), y));
}

img_t rotate(double angle, img_t const & src){
  auto start = std::chrono::system_clock::now();
  long w_src(src[0].size());
  long h_src(src.size());
  auto xy_c_src(scale(0.5, std::make_tuple(w_src, h_src)));
  auto src_to_dest= [angle, xy_c_src](auto xy){return rotate_around(-angle, xy_c_src, xy);};
  long x_min_dest= std::numeric_limits<long>::max(), x_max_dest=std::numeric_limits<long>::min()
    , y_min_dest=std::numeric_limits<long>::max(), y_max_dest= std::numeric_limits<long>::min();
  
  std::tie(x_min_dest, x_max_dest, y_min_dest, y_max_dest)= update_min_max(std::make_tuple(x_min_dest, x_max_dest, y_min_dest, y_max_dest)
									   , src_to_dest(std::make_tuple(0L,0L)));
  std::tie(x_min_dest, x_max_dest, y_min_dest, y_max_dest)= update_min_max(std::make_tuple(x_min_dest, x_max_dest, y_min_dest, y_max_dest)
									   , src_to_dest(std::make_tuple(w_src,0L)));
  std::tie(x_min_dest, x_max_dest, y_min_dest, y_max_dest)= update_min_max(std::make_tuple(x_min_dest, x_max_dest, y_min_dest, y_max_dest)
									   , src_to_dest(std::make_tuple(w_src, h_src)));
  std::tie(x_min_dest, x_max_dest, y_min_dest, y_max_dest)= update_min_max(std::make_tuple(x_min_dest, x_max_dest, y_min_dest, y_max_dest)
									   , src_to_dest(std::make_tuple(0L, h_src)));

  long  w_dest= x_max_dest - x_min_dest;
  long  h_dest= y_max_dest - y_min_dest;

  auto is_in_src=[h_src, w_src](auto r, auto c){return (r >= 0) && (r < h_src) && ( c >= 0) && (c < w_src);};
  auto default_color= decltype(src[0][0]){0,0,0};

#if 0
  img_t dest(h_dest, std::decay<decltype(src[0])>::type {static_cast<std::size_t>(w_dest)});
  for(long r_dest= 0; static_cast<std::size_t>(r_dest) != dest.size(); ++r_dest){
    for(long c_dest= 0; static_cast<std::size_t>(c_dest) != dest[r_dest].size(); ++c_dest){
	long r_src, c_src;
	std::tie(c_src, r_src)= rotate_around(angle, xy_c_src, std::make_tuple(c_dest + x_min_dest
										,r_dest + y_min_dest));
	dest[r_dest][c_dest]= is_in_src(r_src, c_src) ? src[r_src][c_src] : default_color;
      }
  }
#endif
  img_t dest;
  for(long r_dest=0; r_dest != h_dest; ++r_dest){
    dest.push_back(std::decay<decltype(dest[0])>::type{});
    for(long c_dest=0; c_dest != w_dest; ++c_dest){
      	long r_src, c_src;
	std::tie(c_src, r_src)= rotate_around(angle, xy_c_src, std::make_tuple(c_dest + x_min_dest
										,r_dest + y_min_dest));
      dest.back().push_back(is_in_src(r_src, c_src) ? src[r_src][c_src] : default_color);
    }
  }
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cerr<< "rotation of "<< w_src << "x"<< h_src<<" to " << w_dest << "x" << h_dest << " in " << elapsed_seconds.count() << "s.\n";
  return dest;
}
constexpr double const_pi() { return std::acos(-1.); }


int main(int argc, char* argv[]){
  double const angle= (argc >1)
    ? const_pi()*std::stod(argv[1])/180.
    : const_pi()/4.;
  
  img_t img;
  std::cin >> img;
  std::cout << rotate(angle, img);
  return 0;
}
